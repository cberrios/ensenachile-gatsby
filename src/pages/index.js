import * as React from "react";
import Header from "../layout/Header";
import Footer from "../layout/Footer";
import Hero from "../components/home/Hero";
import Helmet from "react-helmet";

import "../styles/app.scss";

// markup
const IndexPage = () => {
  return (
    <>
      <Helmet title="Enseña Chile" defer={false} />

      <Header />

      <Hero />

      <Footer />
    </>
  );
};

export default IndexPage;
