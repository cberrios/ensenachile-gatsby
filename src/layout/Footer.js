import * as React from "react";
import { Link } from "gatsby";
import "../styles/footer.scss";

// markup
const Footer = () => {
  return (
    <footer>
      <div className="container">
        <div className="footer-top">
          <div className="col">
            <img
              src="https://www.niceapp.cl/ensena-chile-front/assets/img/logo-ech-footer.png"
              alt="footer"
            />
          </div>

          <div className="col">
            <p>+98 mil estudiantes impactados</p>
            <p>183 Pech en sala</p>
            <p>8 Regiones</p>
          </div>

          <div className="col">
            <p>+9000 postulaciones en 10 años</p>
            <p>437 Alumni</p>
          </div>
        </div>

        <div className="footer-main">
          <div className="col">
            <img
              src="https://www.niceapp.cl/ensena-chile-front/assets/img/logo-ech.png"
              alt="Enseña Chile"
            />
          </div>

          <div className="col">
            <nav>
              <ul>
                <li>
                  <Link to="/">Lo que hacemos</Link>
                </li>
                <li>
                  <Link to="/">Quiénes somos</Link>
                </li>
                <li>
                  <Link to="/">Nuestro Impacto</Link>
                </li>
                <li>
                  <Link to="/">Testimonios</Link>
                </li>
                <li>
                  <Link to="/">Súmate a la red</Link>
                </li>
              </ul>
            </nav>
          </div>
        </div>

        <div className="footer-disclaimer">
          <nav>
            <ul>
              <li>
                <Link to="/">Términos y condiciones</Link>
              </li>
              <li>
                <Link to="/">Todos los derechos reservados</Link>
              </li>
              <li>
                <Link to="/">Integrante de la Red Global de Educación </Link>
              </li>
            </ul>
          </nav>
        </div>

        <div className="footer-bottom">
          <p>Actualizado el Agosto, 2021</p>

          <a
            href="https://www.ilogica.cl/"
            className="ilogica"
            target="_blank"
            rel="noreferrer"
          >
            <img
              src="https://www.niceapp.cl/ensena-chile-front/assets/img/logo-ilogica.png"
              alt="Ilógica"
            />
          </a>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
