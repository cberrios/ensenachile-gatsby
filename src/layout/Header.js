import React from "react";
import { Link } from "gatsby";
import "../styles/header.scss";

// markup
const Header = () => {
  return (
    <header>
      <div className="container">
        <Link className="logo" to="/">
          <img
            src="https://www.niceapp.cl/ensena-chile-front/assets/img/logo-ensena-chile.png"
            alt="Enseña Chile"
          />
        </Link>

        <nav>
          <ul>
            <li>
              <Link to="/">Lo que hacemos</Link>
            </li>
            <li>
              <Link to="/">Quiénes somos</Link>
            </li>
            <li>
              <Link to="/">Nuestro Impacto</Link>
            </li>
            <li>
              <Link to="/">Testimonios</Link>
            </li>
            <li>
              <Link to="/">Súmate a la red</Link>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;
