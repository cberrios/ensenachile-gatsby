import * as React from "react";
import { Link } from "gatsby";
import "./hero.scss";

// markup
const Hero = () => {
  return (
    <section className="hero-home">
      <div className="container">
        <div className="data">
          <h2>¿Quieres cambiar el sistema? Empieza en una sala de clases</h2>
          <p>
            Trabajamos para que todos los niños tengan la oportunidad de
            desarrollar su potencial, sin que su origen determine su capacidad
            de soñar y alcanzar esos sueños.
          </p>
          <Link className="button" to="/">
            ¡Quiero ser parte!
          </Link>
        </div>
      </div>
      <div className="bg-video">
        <img
          src="http://unsplash.it/g/1920/800?random&gravity=center"
          alt="Bg"
        />
      </div>
    </section>
  );
};

export default Hero;
